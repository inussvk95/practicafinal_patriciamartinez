﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour {

    public Transform BalaSpawner;
    public GameObject BalaPrefab;
    Animator myAnimator;
     
	void Start ()
    {
        myAnimator = GetComponent<Animator>();
	}
	
	
	void Update ()
    {
        Disparando();
	}
     
     public void Disparando()
    {
        if (Input.GetButtonDown("Fire1") && Movimiento.Muerto == false)
        {
            myAnimator.SetBool("Attack", true);
            Instantiate(BalaPrefab, BalaSpawner.position, BalaSpawner.rotation);
        }
        else if (Input.GetButtonUp("Fire1")&& Movimiento.Muerto == false)
        {
            myAnimator.SetBool("Attack", false);
        }
        
    }
}
