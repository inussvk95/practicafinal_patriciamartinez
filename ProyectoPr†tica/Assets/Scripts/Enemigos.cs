﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigos : MonoBehaviour {

    //private float DestruirTiempo = 5.5f;
    public int OrbesEnemigo = 1;
    public GameObject Orbe;
   /* IEnumerator TiempoDestruirEnemigo()
    {
        yield return new WaitForSeconds(DestruirTiempo);
    }*/

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemigo" || collision.gameObject.tag == "Enemigo2")
        {
            collision.GetComponent<Animator>().SetBool("Die", true);
            //StartCoroutine(TiempoDestruirEnemigo());
            for (int i=0; i<OrbesEnemigo; i++)
            {
                Instantiate(Orbe, new Vector2(collision.transform.position.x, collision.transform.position.y), Quaternion.identity);
            }
            Destroy(collision.gameObject);
            Destroy(gameObject);


        
            
        }
       /* if (collision.gameObject.tag == "Enemigo2")
        {
           
            Destroy(collision.gameObject); 
            Destroy(gameObject);
        }*/
    }


    void Start ()
    {
        
    }
	
	
	void Update ()
    {
		
	}
}
