﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovDisparo : MonoBehaviour {

    public GameObject player;
    private Transform playerTrans;
    private Rigidbody2D myRigidbody;
    public float VelBala;
    public float VidaBala;


    private void Awake()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerTrans = player.transform;
    }



    void Start ()
    {
        if (playerTrans.localScale.x > 0)
        {
            myRigidbody.velocity = new Vector2(VelBala, myRigidbody.velocity.y);
            transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        }
        else 
        {
            myRigidbody.velocity = new Vector2(-VelBala, myRigidbody.velocity.y);
            transform.localScale = new Vector3(-0.2f, 0.2f, 0.2f);
        }
	}
	
	
	void Update ()
    {
        Destroy(gameObject, VidaBala);
	}
}
