﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour {


    public int Velocidad;
    public float FuerzaSalto = 100f;
    bool Suelo;
    public static bool Muerto = false;
    Animator myAnimator;
    public static int Vidas = 6;
    public static int Orbes = 0;
    GameObject TextOrbes;
	void Start ()
    {
        myAnimator = GetComponent<Animator>();
        Suelo = true;
        myAnimator.SetBool("Idle", true);
        TextOrbes = GameObject.Find("Orbes");
	}

    public void Salto()
    {
            Suelo = false;
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, FuerzaSalto));
            myAnimator.SetBool("Jump", true);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == ("Player") || collision.gameObject.tag == ("Plataforma") || collision.gameObject.tag == ("Suelo"))
        {
            Suelo = true;
            myAnimator.SetBool("Jump", false);
        }
        if(collision.gameObject.tag==("Orbe"))

        {
            Orbes++;
            TextOrbes.GetComponent<UnityEngine.UI.Text>().text = "Orbes: " + Orbes;
            Destroy(collision.gameObject);
        }
    }

    void Update ()
    {


        if (Input.GetKey(KeyCode.A) && !Muerto)
        {
            transform.localScale = new Vector3(-0.4f, 0.4f, 0);
            transform.Translate(-Time.deltaTime * Velocidad, 0, 0);
            myAnimator.SetBool("Walk", true);
            myAnimator.SetBool("Idle", false);
        }

        if (Input.GetKey(KeyCode.D) && !Muerto)
        {
            transform.localScale = new Vector3(0.4f, 0.4f, 0);
            transform.Translate(Time.deltaTime * Velocidad, 0, 0);
            myAnimator.SetBool("Walk", true);
            myAnimator.SetBool("Idle", false);
        }

        if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
        {
            myAnimator.SetBool("Walk", false);

        }

        if (Input.GetKeyDown(KeyCode.Space) && Suelo == true && !Muerto) 
        {
            Salto();
        }

    }
}
