﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Muerto : MonoBehaviour {
    GameObject Player;

    private void OnCollisionStay2D(Collision2D collision)
    {
        if(collision.gameObject.tag == ("Player"))
        {
            Movimiento.Vidas = Movimiento.Vidas - 1;
            //StartCoroutine("TimeForDamage");
            if(Movimiento.Vidas <= 0)
            {
                Player.GetComponent<Animator>().SetBool("Die", true);
                Player.GetComponent<Animator>().SetBool("Idle", false);
                Movimiento.Muerto = true;
            }
        }
    }




    void Start ()
    {
        Player = GameObject.Find("Player");
	}
	
	
	void Update ()
    {
		
	}
}
