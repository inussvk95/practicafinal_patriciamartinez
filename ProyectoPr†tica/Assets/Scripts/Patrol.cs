﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour {

    public float VelEnemigo;
    private bool MovDerecha = true;
    public Transform EnSuelo;
    private float Distancia;


	void Update ()
    {
        transform.Translate(Vector2.right * VelEnemigo * Time.deltaTime);
        RaycastHit2D SueloInfo = Physics2D.Raycast(EnSuelo.position, Vector2.down, Distancia);

        if(SueloInfo.collider == false)
        {
            if(MovDerecha == true)
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                MovDerecha = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                MovDerecha = true;
            }
        }
	}
}
