﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampas : MonoBehaviour {

    Animator myAnimator;


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Trampa")
        {
            myAnimator.SetBool("Die", true);
            myAnimator.SetBool("Idle", false);
            Movimiento.Muerto = true;
            
        }

    }

    void Start ()
    {
        myAnimator = GetComponent<Animator>();
        myAnimator.SetBool("Die", false);
	}

	void Update ()
    {
        
	}
}
